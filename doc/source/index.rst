.. raw:: html

  <p>
  <a href="https://badge.fury.io/py/hiphive"><img src="https://badge.fury.io/py/hiphive.svg" alt="PyPI version" height="18"></a>
  </p>


:program:`hiPhive` — High-order force constants for the masses
**************************************************************

:program:`hiPhive` is a tool for efficiently extracting high-order force
constants from atomistic simulations, most commonly density functional theory
calculations. It has been implemented in the form of a Python library, which
allows it to be readily integrated with many first-principles codes and
analysis tools accessible in Python.

The following snippet illustrates the construction of a force constant
potential object (``fcp``), which  can be subsequently used to run molecular
dynamics (MD) simulations or analyzed using e.g., `phonopy
<https://atztogo.github.io/phonopy/>`_, `phono3py
<https://atztogo.github.io/phono3py/>`_, or `shengBTE
<http://www.shengbte.org/>`_::

    cs = ClusterSpace(ideal_cell, cutoffs=[6.0, 4.5])
    sc = StructureContainer(cs)
    for atoms in list_of_training_structures:
        sc.add_structure(atoms)
    opt = Optimizer(sc.get_fit_data())
    opt.train()
    fcp = ForceConstantPotential(cs, opt.parameters)

:program:`hiphive` has been developed by
`Fredrik Eriksson <https://materialsmodeling.org/people/fredrik-eriksson/>`_,
`Erik Fransson <https://www.chalmers.se/en/staff/Pages/erikfr.aspx>`_, and
`Paul Erhart <https://materialsmodeling.org/people/paul-erhart/>`_
at the
`Department of Physics <https://www.chalmers.se/en/departments/physics/Pages/default.aspx>`_
of `Chalmers University of Technology <https://www.chalmers.se/>`_ in
Gothenburg, Sweden. Please consult the :ref:`credits page <credits>` for
information on how to cite :program:`hiphive`.

:program:`hiPhive` and its development are hosted on `gitlab
<https://gitlab.com/materials-modeling/hiphive>`_. Bugs and feature
requests are ideally submitted via the `gitlab issue tracker
<https://gitlab.com/materials-modeling/hiphive/issues>`_. If you do
not have a gitlab account you also send questions by email to
hiphive@materialsmodeling.org.

.. toctree::
   :maxdepth: 2
   :caption: Main

   background/index
   installation
   tutorial/index
   advanced_topics/index
   faq
   credits

.. toctree::
   :maxdepth: 2
   :caption: Function reference

   moduleref/index

.. toctree::
   :maxdepth: 2
   :caption: Backmatter

   bibliography
   glossary
   genindex
