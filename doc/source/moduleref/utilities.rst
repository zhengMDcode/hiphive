Other functions
===============


.. _self_consistent_phonons:

.. index::
   single: Self-consistent phonons

Self-consistent phonons
-----------------------

.. automodule:: hiphive.self_consistent_phonons
   :members:
   :undoc-members:



.. index::
   single: Function reference; Utilities
   single: Class reference; Shell

Utilities
---------

.. automodule:: hiphive.utilities
   :members:
   :undoc-members:



Enforcing rotational sum rules
------------------------------

.. autofunction:: hiphive.core.rotational_constraints.enforce_rotational_sum_rules
   :noindex:
